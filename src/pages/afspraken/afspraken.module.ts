import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AfsprakenPage } from './afspraken';

@NgModule({
  declarations: [
    AfsprakenPage,
  ],
  imports: [
    IonicPageModule.forChild(AfsprakenPage),
  ],
})
export class AfsprakenPageModule {}

