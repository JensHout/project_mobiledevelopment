import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/*import { NativeStorage } from '@ionic-native/native-storage';*/

/**
 * Generated class for the AfsprakenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-afspraken',
  templateUrl: 'afspraken.html',
})
export class AfsprakenPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  /*
  this.nativeStorage.getItem('afspraak').then(
    data => console.log(data)
  );
  */


  ionViewDidLoad() {
    console.log('ionViewDidLoad AfsprakenPage');
  }

}
