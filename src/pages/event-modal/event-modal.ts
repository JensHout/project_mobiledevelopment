import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import * as moment from 'moment';
/*import { NativeStorage } from '@ionic-native/native-storage';*/


@IonicPage()
@Component({
  selector: 'page-event-modal',
  templateUrl: 'event-modal.html',
})
export class EventModalPage {

  event = {startTime: new Date().toISOString(), endTime: new Date().toISOString(), allDay: false}
  minDate = new Date().toISOString();

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private alertCtrl: AlertController) {
    let preselectedDate = moment(this.navParams.get('SelectedDay')).format();
    this.event.startTime = preselectedDate;
    this.event.endTime = preselectedDate;

  }

  save() {
    this.viewCtrl.dismiss(this.event);

    //this.nativeStorage.setItem('afspraak', {property: this.event})

    let alert = this.alertCtrl.create({
      title: 'Voltooid!',
      subTitle: 'Uw afspraak werd opgeslagen. Klik op OK om terug naar agenda te gaan.',
      buttons: ['OK']
    })
    alert.present();
  }

  cancel(){
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventModalPage');
  }

}
