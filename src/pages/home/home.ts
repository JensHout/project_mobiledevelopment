import { Component } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';
import * as moment from 'moment';

import {AfsprakenPage} from '../afspraken/afspraken';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  eventSource = [];
  viewTitle: string;
  selectedDay = new Date();

  calendar = {
    mode: 'month',
    currentDate: new Date()
  };

  constructor(public navCtrl: NavController, private modalCtrl: ModalController, private alertCtrl: AlertController) {

  }

  showEvents(){
    this.navCtrl.push(AfsprakenPage);
  }

  addEvent() {
    let modal = this.modalCtrl.create('EventModalPage', {selectedDay: this.selectedDay});
    modal.present();
    modal.onDidDismiss(data => {
      if(data) {
        let eventData = data;

        eventData.startTime = new Date(data.startTime);
        eventData.endTime = new Date(data.endTime);

        let events = this.eventSource;
        events.push(eventData);
        this.eventSource = [];
        setTimeout(() => {
          this.eventSource = events;
        });
      }
    });
  }

  onViewTitleChanged(title){
    this.viewTitle = title;
  }

  onEventSelected(event) {
    let start = moment(event.startTime).format('LLLL');
    let end = moment(event.endTime).format('LLLL');

    let alert = this.alertCtrl.create({
      title: '' + event.title,
      subTitle: 'Van: ' + start + '<br>Tot: ' + end + '<br>Locatie: ' + event.locatie + '<br>Notitie: ' + event.notitie,
      buttons: [
        {
          text: 'Close',
          role: 'cancel'
        },
        {
          text: 'Delete',
          handler: () => {this.deleteEvent(event)}
        }
      ]
    })
    alert.present();
  }

  deleteEvent(event){
    event.deleteEvent(event);
  }

  onTimeSelected(ev) {
    this.selectedDay = ev.selectedTime;
  }

}
